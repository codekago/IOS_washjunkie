$(document).ready(function($) {
    var $$ = Dom7;
    
    
    window.app = new Framework7({
        popup: {
            closeByBackdropClick: false,
          },
          panel: {
            swipe: 'left',
            leftBreakpoint: 768,
            rightBreakpoint: 1440,
          },
          searchbar:{

          },
          smartSelect: {
            pageTitle: 'Select Option',
            openIn: 'popup',
          },
          popover: {
            closeByBackdropClick: false,
          },
          
            
           
    });

    window.loader = function(type) {
        if (type === 'show') {
            app.preloader.show();
        }else {
            app.preloader.hide();
        }
    }
    
    var searchbar = app.searchbar.create({
        el: '.searchbar',
        on: {
          enable: function () {
            console.log('Searchbar enabled')
          }
        }
    });
    var smartSelect = app.smartSelect.create({
        on: {
          opened: function () {
            console.log('Smart select opened')
          }
        }
      });
    app.preloader.hide();

    var smartSelect = app.smartSelect.open()
    
    window.history = () =>{
        const history = document.querySelector('history');
        history.addEventListener('click',() =>{
            window.location.href = '#/history';
        })
    }
    window.panel = app.panel.create({
        el: '.panel-left',
    });
    const history = document.getElementById('history'),
        wallet = document.getElementById('wallet');
        history.addEventListener('click', ()=>{
            app.preloader.show();
            setTimeout(() => {
                location.href = '#/history';
                app.preloader.hide();                
            }, 500);
        });
        wallet.addEventListener('click', () =>{
            app.preloader.show();
            setTimeout(() => {
                location.href = '#/wallet';
                app.preloader.hide();
                
            }, 500);
        });

        // Create dynamic Popover
        var dynamicPopover = app.popover.create({
            targetEl: 'a.dynamic-popover',
            content: '<div class="popover">'+
                        '<div class="popover-inner">'+
                        '<div class="block">'+
                            '<h3 style="font-weight:bold; text-align:center; font-size:20px">Order for Pickup</h3>'+
                            '<h6 style="font-size:12px;">Select delivery Address.</h6>'+
                            '<select class="select"><option value="">Four lane</option><option value="">Nwaniba</option><option value="">Plaza</option><option value="">Oron Road</option></select>'+
                            '<button class="button col button_login script_btn">submit</button>'+
                            '<p><a href="#" class="link popover-close"><i class="f7-icons">close  </a></p>'+
                        '</div>'+
                        '</div>'+
                    '</div>',
            // Events
            on: {
                open: function (popover) {
                    console.log('Popover open');
                },
                opened: function (popover) {
                    console.log('Popover opened');
                },
            }
        });
        // Events also can be assigned on instance later
        dynamicPopover.on('close', function (popover) {
            console.log('Popover close');
        });
        dynamicPopover.on('closed', function (popover) {
            console.log('Popover closed');
        });
        
        // Open dynamic popover
        $$('.dynamic-popover').on('click', function () {
            dynamicPopover.open();
        });
        
        var smartSelect = app.smartSelect.create({
            on: {
              opened: function () {
                console.log('Smart select opened')
              }
            }
          });
          app.smartSelect.open('.smartSelect');
        var smartSelect = app.smartSelect.get('.smart-select');
        window.callFun = () => {
            const more = document.getElementById('more').style.display = "none",
                camera = document.getElementById('camera').style.display ="none",
                footer = document.querySelector('#footer').style.display = "none";
                header = document.querySelector('#header').style.display = "none"; 
        }
    // window.page = () =>{
    //     document.querySelector('.md .navbar ~ .page-content').style.padding = "0";
    // }
        
    window.upload = () =>{
        document.getElementById("image_src").click();
    }
    window.back = () =>{
        window.history.back();
    }

    
        const edit = document.getElementById('edit');
            edit.addEventListener('click', ()=>{
                location.href='#/profile';
                app.panel.close();
            });
    

            $(function () {
                $(":file").change(function () {
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });
            function imageIsLoaded(e) {
                $('.profile-img').attr('src', e.target.result);
                
            };
        window.
            config = {
                apiKey: "AIzaSyAXDhqx16ce-6xgxCzBxzafXTQzYaWSuVg",
                authDomain: "washjunkie-ebeae.firebaseapp.com",
                databaseURL: "https://washjunkie-ebeae.firebaseio.com",
                projectId: "washjunkie-ebeae",
                storageBucket: "washjunkie-ebeae.appspot.com",
                messagingSenderId: "569168782225"
            };
            
        

});