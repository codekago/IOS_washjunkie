washjunkie.controller('profileController',['$scope', ($scope) =>{
    window.page = () =>{
        document.querySelector('.page-content ').style.padding = "56px 0 0 0 ";
    }
    console.log('Profile');
    window.callFun = ()=>{
        const more = document.getElementById('more').style.display = "none",
        camera = document.getElementById('camera').style.display ="block",
        footer = document.querySelector('#footer').style.display = "block";
    }
    var smartSelect = app.smartSelect.get('.smart-select');

    $(function () {
        $(":file").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    
    function imageIsLoaded(e) {
        $('#myImg').attr('src', e.target.result);
        $('.profile-img').attr('src', e.target.result);
    };
    

    window.callFun();
    window.page();
}]);