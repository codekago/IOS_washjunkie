
washjunkie.controller('historyController',['$scope', function(scope){
  window.callFun = ()=>{
    const more = document.getElementById('more').style.display = "block",
          camera = document.getElementById('camera').style.display ="none",
          footer = document.querySelector('#footer').style.display = "block",
          header = document.querySelector('#header').style.display = "block";
  }

  let userPhoneNumber = localStorage.getItem('phone_number');
	if (userPhoneNumber.startsWith('+234', 0)) {
		let rawNum = userPhoneNumber;
		userPhoneNumber = rawNum.slice(4, rawNum.length);
  }
  
  const database = firebase.database();
	const oderHistoryTree = document.querySelector('.searchbar-found ul');
	const orderHistoryArr = [];
	const navbarName = document.getElementById('navbar-name');
	const navbarNumber = document.getElementById('navbar-num');
	let orderHistory = {};

	const selectOtherAddress = document.getElementById('select-address');
  const placeOrderBtn = document.getElementById('place-order-btn');
  
    console.log('history');
    var searchbar = app.searchbar.create({
        el: '.searchbar',
        searchContainer:'.list',
        searchIn: '.item-title',
        on: {
          enable: function () {
            console.log('Searchbar enabled')
          },
          search(sb,query,previousQuery){
          }
        }
      })
   window.callFun();
   

   let userDB = database.ref('test-users');
	userDB.orderByChild('phone').equalTo(userPhoneNumber).on('value', snapshot => {
		console.log(snapshot.val());
		let data = snapshot.val();
		let uuid = Object.keys(data)[0];
		let addressOptions = [];
		for (const key in data) {
			if (data.hasOwnProperty(key)) {
				const element = data[key];
				navbarName.innerText = element.name;
				navbarNumber.innerText = userPhoneNumber || element.phone;
				orderHistory = element.order;
				selectOtherAddress.insertAdjacentHTML('beforeend', `<option val='${element.address}'>${element.address}</option>`);
			}
		}
		for (const key in orderHistory) {
			if (orderHistory.hasOwnProperty(key)) {
				const element = orderHistory[key];
				orderHistoryArr.push(element);
			}
		}
		if (data[uuid].otherAddress) {
			for (const key in data[uuid].otherAddress) {
				if (data[uuid].otherAddress.hasOwnProperty(key)) {
					const element = data[uuid].otherAddress[key];
					addressOptions.push(element);
				}
			}
			for (let i = 0; i < addressOptions.length; i++) {
				const element = addressOptions[i];
				selectOtherAddress.insertAdjacentHTML('beforeend', `<option val='${element}'>${element}</option>`);
			}
		}
		oderHistoryTree.innerHTML = '';
		for (let i = orderHistoryArr.length - 1; i >= 0; i--) {
			const element = orderHistoryArr[i];
			let _date = element.date;
			let formattedDate = _date.split(',');
			let li = `<li class='item-content' id='${element.orderNum}'><div class='item-inner'><div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%; padding-top: 5px;"><p>Order Num: ${element.orderNum}<br><span>Cost: &#8358;${element.cost}</span><br><span class='_status' style='text-transform: capitalize;'>${element.status}</span></p><p style='text-align: right; padding-top: 10px; padding-bottom: 10px;'>${formattedDate[1]}<br><span>${formattedDate[0]}</p></div></div></li>`;
			oderHistoryTree.innerHTML += li;
		}

		let allStatus = document.querySelectorAll('._status');
		for (let i = 0; i < allStatus.length; i++) {
			const element = allStatus[i];
			switch (element.innerHTML) {
				case 'pending':
					element.style.color = '#03C5C9';
					break;
				case 'delivered':
					element.style.color = '#69BB0F';
					break;
				case 'ready':
					element.style.color = '#026d02';
					break;
				case 'confirmed':
					element.style.color = '#5590f6';
					break;
				default:
					break;
			}
		}
	});

	let unknownOrderDBRef = database.ref('unknownOrder');
	unknownOrderDBRef.orderByChild('phone').equalTo(userPhoneNumber).on('value', snapshot => {
		let data = snapshot.val();
		let pendingOrderArr = [];
		for (const key in data) {
			if (data.hasOwnProperty(key)) {
				const element = data[key];
				pendingOrderArr.push(element);
			}
		}

		let orderStatusDiv = document.querySelector('.pending-order ul li .item-inner');
		orderStatusDiv.innerHTML = '';
		let currentOrder = pendingOrderArr[pendingOrderArr.length - 1];
		let formattedDate = currentOrder.date.split(',');

		let appendOrder = `<div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Order Number: </p><p style='text-align: right;'>${currentOrder.orderNum}</p></div><div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Date: </p><p style='text-align: right;'>${formattedDate[0]}</p></div><div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Time: </p><p style='text-align: right'>${formattedDate[1]}</p></div><div class='item-title' style="display: grid; grid-template-columns: 1fr 1fr; width: 100%;"><p>Status: </p><p style='text-align: right; text-transform: capitalize;'>${currentOrder.status}</p></div>`;

		orderStatusDiv.insertAdjacentHTML('beforeend', appendOrder);
	});

	function createOrderNum() {
		var num = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for (var i = 0; i < 5; i++)
			num += possible.charAt(Math.floor(Math.random() * possible.length));
		return num;
	}

	placeOrderBtn.addEventListener('click', () => {
		console.log('log');
		userDB.orderByChild('phone').equalTo(userPhoneNumber).once('value', snapshot => {
			let data = snapshot.val();
			let name;
			let today = new Date().toLocaleString('en-NG', { timezone: 'WAT' });
			let orderNum = createOrderNum();
			let dbRef = database.ref('unknownOrder');

			for (const key in data) {
				if (data.hasOwnProperty(key)) {
					const element = data[key];
					name = element.name;
				}
			}
			if (!selectOtherAddress.value) {
				swal({ type: 'error', title: 'Empty input field', text: 'Order can\'t be made. Please select an address' });
			} else {
				dbRef.push({
					phone: userPhoneNumber,
					name,
					orderNum,
					date: today,
					status: 'pending',
					deliverTo: selectOtherAddress.value
				}).then(() => { swal('Order successfuly made. We shall reach out to you soon for confirmation') });
			}
		})
	});
      

}]);
