washjunkie.controller('beginController', ['$scope', function($scope) {
    window.page = () =>{
        document.querySelector('.ios .navbar ~ .page-content').style.padding = "0 0 0 0 ";
    }
    window.callFun();
    console.log('begin');
    
    const start = document.querySelector('#start');
    start.addEventListener('click', () =>{
        app.preloader.show();
        setTimeout(() => {
            app.preloader.hide();
            window.location.href = "#/login"    
        }, 500);
    });
    window.callFun();
    window.page();
}]);