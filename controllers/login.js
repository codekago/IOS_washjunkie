washjunkie.controller('loginController', ['$scope', function($scope) {
    console.log("login");
   window.callFun();
   window.page = () =>{
    document.querySelector('.ios .navbar ~ .page-content').style.padding = "0 0 0 0 ";
    }
    
    var $$ = Dom7;
    const login = document.querySelector('#login')
        
        firebase.auth().languageCode ='en';
        login.addEventListener('click',() => {
            var phone_number = document.getElementById('phone_number').value;
            if(phone_number === "" || phone_number.length < 9){
                swal('Ooops...', 'Please Enter a valid number!', 'error');
                console.log('incomplete')
            }else{
                if(phone_number.length > 10){
                    var phone_number = "+234" + document.getElementById('phone_number').value;
                    localStorage.setItem('phone_number', phone_number);
                    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('login', {
                        'size':"invisible",
                        'callback': function(response){
                            console.log('callback');
                        }
                    });
                    recaptchaVerifier.render().then((widgetId)=>{
                        window.recaptchaWidgetId = widgetId;
                        
                    });
                    let appVerifier = window.recaptchaVerifier;
                    firebase.auth().signInWithPhoneNumber(phone_number, appVerifier)
                        .then((confirmationResult) =>{
                            console.log(confirmationResult);
                            window.confirmationResult = confirmationResult;
                                app.preloader.show();
                            setTimeout(() =>{
                                app.preloader.hide();
                                window.location.href = ('#/verify');
                            }, 3000)
                            
                        }).catch((error) =>{
                            if(error){
                                window.location.href = ('#login');
                            }
                        })
                }
            }
        });
        window.callFun();
        window.page();


  
}]);

